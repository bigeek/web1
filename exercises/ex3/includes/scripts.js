$(document).ready(function () {

    let boxSize = 80;
    let box_id = 0;
    let rand = 0;
    let alphabet = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'.split('');
    let alphabetPos = []; // says where location
    let alphabetDouble = []; // says where doubles
    let opened = 0;
    let openedValue = 0;

    for (let i = 0; i < 7 && box_id < 52; i++) {
        CreateBox();
    }

    $(".create_box").click(function () {
        for (let i = 0; i < 3 && box_id < 52; i++) {
            CreateBox();
            }
        if(box_id < 52){
            console.log("Array full");
        }
    })

    function CreateBox() {
            let box = $('<div>').attr("class", "box" + " box_" + box_id).css({
                "height": boxSize + "px",
                "width": boxSize + "px"
            });
            
            $('main').append(box);
            boxSize += 20;
            alphabetPos[box_id] = CreateLetter(0, 3 + rand); //0-25
            box_id++;
            rand= rand + 0.70;
    }


    $("main").on("click", ".box", function () {
        for (i = 0; i < 54; i++) {
            if ($(this).hasClass("box_" + i)) {
                let num = $('<h1>').attr("class", "h1").text(alphabet[alphabetPos[i]]);
                $(this).append(num);
                opened++;
                if (opened == 2){
                    if (alphabet[alphabetPos[openedValue]] == alphabet[alphabetPos[i]]){
                        $(".box_"+openedValue).css({background : '#03A9F4', transition: 'background-color 0.5s ease'})
                        $(".box_"+i).css({background : '#03A9F4', transition: 'background-color 0.5s ease'})

                        opened = 0;
                        openedValue = -1;
                        break;
                    }
                    else{
                        $(".box_"+openedValue+" .h1").fadeOut(1000, function(){$(this).remove();});
                        $(".box_"+i+" .h1").fadeOut(1000, function(){$(this).remove();});
                    }
                    opened = 0;
                }
                openedValue = i;
                break;
            }
        }
    })

    function CreateLetter(min, max) {
        if (max > 25){max = 25};
        for (;;) {
            let num = Math.floor(Math.random() * (max - min + 1)) + min;
            if (alphabetDouble[num] === undefined) {
                alphabetDouble[num] = 1;
                return num;
            } else if (alphabetDouble[num] == 1) {
                alphabetDouble[num]++;
                return num;
            }
        }
    }
});