<!DOCTYPE html>
<nav>

    <head>
        <title>json</title>
        <meta charset="UTF-8">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    </head>

    <body id="json">
        <header>
</nav>
</nav>
</header>
<main>
    <?php
    $colors = ["red", "blue", "yellow"];
    echo "<p> I love colors " . $colors[0] . " and " . $colors[1] . ", but my sister likes the color " . $colors[2] . "</p>";
    ?>

    <table class="table">
        <thead>
            <tr>
                <?php
                $days = ['Sunday', 'Monday', 'Friday', 'Saturday'];
                foreach ($days as $day) {
                    echo '<th scope="col">' . $day . '</th>';
                }
                ?>
            </tr>
        </thead>
        <tbody>
            <tr>
                <th scope="row">1</th>
                <td>Mark</td>
                <td>Otto</td>
                <td>@mdo</td>
            </tr>
            <tr>
                <th scope="row">2</th>
                <td>Jacob</td>
                <td>Thornton</td>
                <td>@fat</td>
            </tr>
            <tr>
                <th scope="row">3</th>
                <td>Larry</td>
                <td>the Bird</td>
                <td>@twitter</td>
            </tr>
        </tbody>
    </table>

    <div class="row">
        <?php
        $IMGarr = [
            ['https://dummyimage.com/250/ffffff/000000', '1', 'black', 'This is text'],
            ['https://dummyimage.com/250/FF0000/000000', '2', 'blue', 'Simple Text'],
            ['https://dummyimage.com/250/00FF00/000000', '3', 'red', 'Hey yo'],
            ['https://dummyimage.com/250/FF00FF/000000', '4', 'purple', 'Just a picture test'],
        ];

        foreach ($IMGarr as $IMGdet) {
            echo '<div class="card" style="width: 18rem;">
                   <img class="card-img-top" src=' . $IMGdet[0] . ' alt=' . $IMGdet[1] . '>
                   <div class="card-body">
                     <h5 class="card-title">' . $IMGdet[2] . '</h5>
                     <p class="card-text">' . $IMGdet[3] . '</p>
                   </div>
              </div>';
        }
        ?>
    </div>
    <br>
    <form action="/calc.php" method="get">
        <select class="custom-select custom-select-lg mb-3" name="number">
            <option selected>Open this select menu</option>
            <option value="0">One</option>
            <option value="1">Two</option>
            <option value="2">Three</option>
            <option value="3">Four</option>
        </select>
        <button type="submit" class="btn btn-primary">Submit</button>
    </form>

</main>
<footer></footer>
</body>